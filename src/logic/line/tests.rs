//use crate::logic::line::GridElementStatus;

#[cfg(test)]
use super::*;

#[test]
fn test_order() {
    assert_eq!(true, GridElement::FREE <  GridElement::BLOCKED);
    assert_eq!(true, GridElement::BLOCKED <  GridElement::USED);
    assert_eq!(true, GridElement::USED <  GridElement::FIXED);
}

#[test]
fn test_get_minimal_marketplace_size() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(1, line.get_minimal_marketplace_size());

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(3, line.get_minimal_marketplace_size());

    let info = vec![1, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(5, line.get_minimal_marketplace_size());

    let info = vec![1, 5, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(9, line.get_minimal_marketplace_size());

    let info = vec![2, 1, 1, 2];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(9, line.get_minimal_marketplace_size());
}

#[test]
fn test_single_not_fixed() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(true, line.single_not_fixed(0));

    let info = vec![1,1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(false, line.single_not_fixed(0));

    let info = vec![1,1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::USED;
    assert_eq!(false, line.single_not_fixed(0));

    let info = vec![1,1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::USED;
    line.rec_info[1].status = RecStatus::FIXED;
    assert_eq!(true, line.single_not_fixed(0));
    assert_eq!(false, line.single_not_fixed(1));

    let info = vec![1,1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    line.rec_info[1].status = RecStatus::FIXED;
    assert_eq!(false, line.single_not_fixed(0));
    assert_eq!(false, line.single_not_fixed(1));

    let info = vec![1,1,1,1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    line.rec_info[1].status = RecStatus::FIXED;
    assert_eq!(false, line.single_not_fixed(2));
    assert_eq!(false, line.single_not_fixed(3));

    let info = vec![1,1,1,1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    line.rec_info[1].status = RecStatus::FIXED;
    line.rec_info[3].status = RecStatus::FIXED;
    assert_eq!(true, line.single_not_fixed(2));
    assert_eq!(false, line.single_not_fixed(3));
}

#[test]
fn test_get_rec_most_left_and_right_start() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(9, line.rec_most_right_start(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(9, line.rec_most_right_start(0));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(2, line.rec_most_left_start(1));
    assert_eq!(7, line.rec_most_right_start(0));
    assert_eq!(9, line.rec_most_right_start(1));

    let info = vec![1, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(2, line.rec_most_left_start(1));
    assert_eq!(4, line.rec_most_left_start(2));
    assert_eq!(5, line.rec_most_right_start(0));
    assert_eq!(7, line.rec_most_right_start(1));
    assert_eq!(9, line.rec_most_right_start(2));

    let info = vec![1, 5, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(2, line.rec_most_left_start(1));
    assert_eq!(8, line.rec_most_left_start(2));
    assert_eq!(1, line.rec_most_right_start(0));
    assert_eq!(7, line.rec_most_right_start(1));
    assert_eq!(9, line.rec_most_right_start(2));

    let info = vec![2, 1, 1, 2];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(3, line.rec_most_left_start(1));
    assert_eq!(5, line.rec_most_left_start(2));
    assert_eq!(7, line.rec_most_left_start(3));
    assert_eq!(2, line.rec_most_right_start(0));
    assert_eq!(4, line.rec_most_right_start(1));
    assert_eq!(6, line.rec_most_right_start(2));
    assert_eq!(9, line.rec_most_right_start(3));

    let info = vec![4, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(5, line.rec_most_left_start(1));
    assert_eq!(3, line.rec_most_right_start(0));
    assert_eq!(9, line.rec_most_right_start(1));

    let info = vec![3, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[3].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(4, line.rec_most_left_start(1));
    assert_eq!(7, line.rec_most_right_start(0));
    assert_eq!(9, line.rec_most_right_start(1));

    let info = vec![3, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[3].borrow_mut() = GridElement::USED;
    assert_eq!(1, line.rec_most_left_start(0));
    assert_eq!(5, line.rec_most_left_start(1));
    assert_eq!(7, line.rec_most_right_start(0));
    assert_eq!(9, line.rec_most_right_start(1));

    let info = vec![1, 3];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[6].borrow_mut() = GridElement::USED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(2, line.rec_most_left_start(1));
    assert_eq!(4, line.rec_most_right_start(0));
    assert_eq!(8, line.rec_most_right_start(1));

    let info = vec![1, 4];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[6].borrow_mut() = GridElement::USED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(3, line.rec_most_left_start(1));
    assert_eq!(4, line.rec_most_right_start(0));
    assert_eq!(9, line.rec_most_right_start(1));

    // M x x M . x m . . .  1 2 1 1
    let info = vec![1, 2, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[5].borrow_mut() = GridElement::BLOCKED;
    *line.elements[6].borrow_mut() = GridElement::USED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(0, line.rec_most_right_start(0));
    assert_eq!(3, line.rec_most_left_start(1));
    assert_eq!(4, line.rec_most_right_start(1));
    assert_eq!(6, line.rec_most_left_start(2));
    assert_eq!(6, line.rec_most_right_start(2));
    assert_eq!(8, line.rec_most_left_start(3));
    assert_eq!(9, line.rec_most_right_start(3));

    // M x x x x M . x . .  1 2
    let info = vec![1, 2];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    *line.elements[3].borrow_mut() = GridElement::BLOCKED;
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    *line.elements[5].borrow_mut() = GridElement::FIXED;
    *line.elements[7].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(0, line.rec_most_left_start(0));
    assert_eq!(5, line.rec_most_right_start(0));
    assert_eq!(5, line.rec_most_left_start(1));
    assert_eq!(9, line.rec_most_right_start(1));
}

#[test]
fn test_get_maximal_space_for_rec() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0, 10), line.max_space_for_rec(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,10), line.max_space_for_rec(0));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,8), line.max_space_for_rec(0));
    assert_eq!((2,8), line.max_space_for_rec(1));

    let info = vec![1, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,6), line.max_space_for_rec(0));
    assert_eq!((2,6), line.max_space_for_rec(1));
    assert_eq!((4,6), line.max_space_for_rec(2));

    let info = vec![1, 5, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,2), line.max_space_for_rec(0));
    assert_eq!((2,6), line.max_space_for_rec(1));
    assert_eq!((8,2), line.max_space_for_rec(2));

    let info = vec![2, 1, 1, 2];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,3), line.max_space_for_rec(0));
    assert_eq!((3,2), line.max_space_for_rec(1));
    assert_eq!((5,2), line.max_space_for_rec(2));
    assert_eq!((7,3), line.max_space_for_rec(3));

    let info = vec![4, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    assert_eq!((0,4), line.max_space_for_rec(0));
    assert_eq!((5,5), line.max_space_for_rec(1));

    let info = vec![1, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,6), line.max_space_for_rec(0));
    assert_eq!((2,6), line.max_space_for_rec(1));
    assert_eq!((4,6), line.max_space_for_rec(2));

    let info = vec![1, 5, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,2), line.max_space_for_rec(0));
    assert_eq!((2,6), line.max_space_for_rec(1));
    assert_eq!((8,2), line.max_space_for_rec(2));

    let info = vec![2, 1, 1, 2];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!((0,3), line.max_space_for_rec(0));
    assert_eq!((3,2), line.max_space_for_rec(1));
    assert_eq!((5,2), line.max_space_for_rec(2));
    assert_eq!((7,3), line.max_space_for_rec(3));

    let info = vec![4, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    assert_eq!((0,4), line.max_space_for_rec(0));
    assert_eq!((5,5), line.max_space_for_rec(1));

    // M x x M . x m . . .  1 2 1 1
    let info = vec![1, 2, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[5].borrow_mut() = GridElement::BLOCKED;
    *line.elements[6].borrow_mut() = GridElement::USED;
    assert_eq!((0,1), line.max_space_for_rec(0));
    assert_eq!((3,2), line.max_space_for_rec(1));
    assert_eq!((6,1), line.max_space_for_rec(2));
    assert_eq!((8,2), line.max_space_for_rec(3));


}

#[test]
fn test_maximal_space_for_rec_from() {
    let lines: Vec<Line> = Vec::new();

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . m . . . . . . . . .  2 1 1
    let info = vec![2, 1, 1];
    let line = Line::new(12, 0, info, &lines);
    assert_eq!((0,8), line.max_space_for_rec(0));
    *line.elements[2].borrow_mut() = GridElement::USED;
    assert_eq!((1,7), line.max_space_for_rec(0));

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . . m . . . . . . . .  2 1 1
    let info = vec![2, 1, 1];
    let line = Line::new(12, 0, info, &lines);
    assert_eq!((0,8), line.max_space_for_rec(0));
    *line.elements[3].borrow_mut() = GridElement::USED;
    assert_eq!((0,8), line.max_space_for_rec(0));

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . x . . . . . . . . .  2 1 1
    let info = vec![2, 1, 1];
    let line = Line::new(12, 0, info, &lines);

    //*line.elements[3].borrow_mut() = GridElementStatus::BLOCKED;
    //*line.elements[4].borrow_mut() = GridElementStatus::BLOCKED;
    //assert_eq!((0,8), line.maximal_space_for_rec(0));
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    //assert_eq!((1,7), line.maximal_space_for_rec(0));
    //assert_eq!((3,2), line.maximal_space_for_rec(1));
    //assert_eq!((6,1), line.maximal_space_for_rec(2);

}

#[test]
fn test_get_overlapping_for_rec() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![5, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((1,4)), line.overlapping_for_rec(0));

    let info = vec![1, 5, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((3,4)), line.overlapping_for_rec(1));

    let info = vec![1, 1, 5];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((5,4)), line.overlapping_for_rec(2));

    let info = vec![4, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(Some((0,4)), line.overlapping_for_rec(0));
    //assert_eq!(0, line.get_overlapping_for_rec(1));

    // M x x M . x m . . .  1 2 1 1
    let info = vec![1, 2, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[5].borrow_mut() = GridElement::BLOCKED;
    *line.elements[6].borrow_mut() = GridElement::USED;
    assert_eq!(Some((0,1)), line.overlapping_for_rec(0));
    assert_eq!(Some((3,2)), line.overlapping_for_rec(1));
}

#[test]
fn test_find_fixed_left() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[2].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_left(0));

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    assert_eq!(Some(1), line.find_fixable_rec_from_left(0));

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::USED;
    assert_eq!(Some(0), line.find_fixable_rec_from_left(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_left(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::USED;
    assert_eq!(Some(0), line.find_fixable_rec_from_left(0));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    *line.elements[3].borrow_mut() = GridElement::USED;
    assert_eq!(Some(1), line.find_fixable_rec_from_left(0));
    assert_eq!(None, line.find_fixable_rec_from_left(1));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::USED;
    *line.elements[3].borrow_mut() = GridElement::USED;
    assert_eq!(Some(0), line.find_fixable_rec_from_left(0));
    assert_eq!(None, line.find_fixable_rec_from_left(1));

    let info = vec![1, 1];
    let mut line = Line::new(10, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    line.rec_info[0].status = RecStatus::FIXED;
    *line.elements[2].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_left(0));
    assert_eq!(Some(2), line.find_fixable_rec_from_left(1));

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // x x . m x . . . . . . .  2 3
    let info = vec![2, 3];
    let line = Line::new(12, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::BLOCKED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[3].borrow_mut() = GridElement::USED;
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(Some(2), line.find_fixable_rec_from_left(0));
    assert_eq!(None, line.find_fixable_rec_from_left(1));

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // m m m m m . m . . m m . 5 1 3
    let info = vec![5, 1, 3];
    let line = Line::new(12, 0, info, &lines);
    *line.elements[0].borrow_mut() = GridElement::USED;
    *line.elements[1].borrow_mut() = GridElement::USED;
    *line.elements[2].borrow_mut() = GridElement::USED;
    *line.elements[3].borrow_mut() = GridElement::USED;
    *line.elements[4].borrow_mut() = GridElement::USED;
    *line.elements[6].borrow_mut() = GridElement::USED;
    *line.elements[9].borrow_mut() = GridElement::USED;
    *line.elements[10].borrow_mut() = GridElement::USED;
    //assert_eq!(None, line.find_fixable_rec_from_right(0));
    assert_eq!(Some(0), line.find_fixable_rec_from_left(0));

}

#[test]
fn test_find_fixable_rec_from_left_to_right() {
    let lines: Vec<Line> = Vec::new();

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // m m m m m . m . . m m . 5 1 3
    let info = vec![5, 1, 3];
    let mut line = Line::new(12, 0, info, &lines);
    for _i in 0..5 {
        *line.elements[_i].borrow_mut() = GridElement::USED;
    }
    *line.elements[6].borrow_mut() = GridElement::USED;
    *line.elements[9].borrow_mut() = GridElement::USED;
    *line.elements[10].borrow_mut() = GridElement::USED;
    line.find_fixable_rec_from_left_to_right();
    for _i in 0..5 {
        assert_eq!(true, *line.elements[_i].borrow() == GridElement::FIXED);
    }
    assert_eq!(true, *line.elements[5].borrow() == GridElement::BLOCKED);
    assert_eq!(true, *line.elements[6].borrow() == GridElement::FIXED);
    assert_eq!(true, *line.elements[7].borrow() == GridElement::BLOCKED);
    assert_eq!(true, *line.elements[9].borrow() == GridElement::USED);
    assert_eq!(true, *line.elements[10].borrow() == GridElement::USED);
}

#[test]
fn test_find_fixed_right() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_right(0));

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[9].borrow_mut() = GridElement::USED;
    assert_eq!(Some(9), line.find_fixable_rec_from_right(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_right(0));

    let info = vec![3];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[9].borrow_mut() = GridElement::USED;
    assert_eq!(Some(9), line.find_fixable_rec_from_right(0));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[1].borrow_mut() = GridElement::USED;
    *line.elements[3].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_right(0));
    assert_eq!(None, line.find_fixable_rec_from_right(1));

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    *line.elements[3].borrow_mut() = GridElement::USED;
    *line.elements[9].borrow_mut() = GridElement::USED;
    assert_eq!(None, line.find_fixable_rec_from_right(0));
    assert_eq!(Some(9), line.find_fixable_rec_from_right(1));

    let info = vec![1, 1];
    let mut line = Line::new(10, 0, info, &lines);
    *line.elements[9].borrow_mut() = GridElement::FIXED;
    line.rec_info[1].status = RecStatus::FIXED;
    *line.elements[7].borrow_mut() = GridElement::USED;
    assert_eq!(Some(7), line.find_fixable_rec_from_right(0));
    assert_eq!(None, line.find_fixable_rec_from_right(1));

    // 0 1 2 3 4 5 6 7 8 9
    // M x x x x M M x . .  1 2
    let info = vec![1, 2];
    let mut  line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    line.rec_info[1].status = RecStatus::FIXED;
    *line.elements[0].borrow_mut() = GridElement::FIXED;
    *line.elements[1].borrow_mut() = GridElement::BLOCKED;
    *line.elements[2].borrow_mut() = GridElement::BLOCKED;
    *line.elements[3].borrow_mut() = GridElement::BLOCKED;
    *line.elements[4].borrow_mut() = GridElement::BLOCKED;
    *line.elements[5].borrow_mut() = GridElement::FIXED;
    *line.elements[6].borrow_mut() = GridElement::FIXED;
    assert_eq!(None, line.find_fixable_rec_from_right(1));

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . . . . . . x m . x x 3 2
    let info = vec![3, 2];
    let line = Line::new(12, 0, info, &lines);
    *line.elements[11].borrow_mut() = GridElement::BLOCKED;
    *line.elements[10].borrow_mut() = GridElement::BLOCKED;
    *line.elements[8].borrow_mut() = GridElement::USED;
    *line.elements[7].borrow_mut() = GridElement::BLOCKED;
    assert_eq!(Some(9), line.find_fixable_rec_from_right(1));
    assert_eq!(None, line.find_fixable_rec_from_right(0));

}

#[test]
fn test_find_fixable_rec_from_right_to_left() {
    let lines: Vec<Line> = Vec::new();

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . m m . . m . m m m m m 5 1 3
    let info = vec![3, 1, 5];
    let mut line = Line::new(12, 0, info, &lines);
    for _i in 7..12 {
        *line.elements[_i].borrow_mut() = GridElement::USED;
    }
    *line.elements[1].borrow_mut() = GridElement::USED;
    *line.elements[2].borrow_mut() = GridElement::USED;
    *line.elements[5].borrow_mut() = GridElement::USED;
    line.find_fixable_rec_from_right_to_left();
    for _i in 7..12 {
        assert_eq!(true, *line.elements[_i].borrow() == GridElement::FIXED);
    }
   // println!("{}",line.to_string());
    assert_eq!(true, *line.elements[4].borrow() == GridElement::BLOCKED);
    assert_eq!(true, *line.elements[5].borrow() == GridElement::FIXED);
    assert_eq!(true, *line.elements[6].borrow() == GridElement::BLOCKED);
    assert_eq!(true, *line.elements[1].borrow() == GridElement::USED);
    assert_eq!(true, *line.elements[2].borrow() == GridElement::USED);
}


#[test]
fn test_get_max_not_fixed_rec_length() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    assert_eq!(None, line.max_not_fixed_rec_length());

    let info = vec![1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((1,1)), line.max_not_fixed_rec_length());

    let info = vec![1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((1,2)), line.max_not_fixed_rec_length());

    let info = vec![3, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((3,1)), line.max_not_fixed_rec_length());

    let info = vec![1, 3];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((3,1)), line.max_not_fixed_rec_length());

    let info = vec![1, 1, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((1,3)), line.max_not_fixed_rec_length());

    let info = vec![1, 2, 1];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((2,1)), line.max_not_fixed_rec_length());

    let info = vec![1, 2, 1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[1].status = RecStatus::FIXED;
    assert_eq!(Some((1,2)), line.max_not_fixed_rec_length());

    let info = vec![2, 2, 2];
    let line = Line::new(10, 0, info, &lines);
    assert_eq!(Some((2,3)), line.max_not_fixed_rec_length());
}

#[test]
fn test_block_left_from_rec() {
    let lines: Vec<Line> = Vec::new();

    let info = vec![1];
    let mut line = Line::new(10, 0, info, &lines);
    line.block_before_rec(0);
    for elem in line.elements.iter_mut() {
        assert_eq!(true, GridElement::FREE == *elem.borrow_mut());
    }

    let info = vec![1];
    let mut line = Line::new(10, 0, info, &lines);
    line.rec_info[0].status = RecStatus::FIXED;
    *line.elements[3].borrow_mut() = GridElement::FIXED;
    line.block_before_rec(0);
    for _i in 0..3 {
        assert_eq!(true, GridElement::BLOCKED == *line.elements[_i].borrow_mut());
    }
    line.block_after_rec(0);
    for _i in 4..10 {
        assert_eq!(true, GridElement::BLOCKED == *line.elements[_i].borrow_mut());
    }

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . . M . . . . . . . .  4
    let info = vec![4];
    let mut line = Line::new(12, 0, info, &lines);
    //line.rec_info[0] = RecStatus::FIXED;
    *line.elements[3].borrow_mut() = GridElement::FIXED;
    line.block_before_rec(0);
    line.block_after_rec(0);
    for _i in 0..3 {
        assert_eq!(true, GridElement::FREE == *line.elements[_i].borrow_mut());
    }
    line.block_after_rec(0);
    for _i in 4..12 {
        assert_eq!(true, GridElement::FREE == *line.elements[_i].borrow_mut());
    }
}

#[test]
fn test_block_front_before_single_from_left() {
    let lines: Vec<Line> = Vec::new();

    // 0 1 2 3 4 5 6 7 8 9 0 1
    // . . . M . . M . . . . .
    let info = vec![4];
    let mut line = Line::new(12, 0, info, &lines);
    *line.elements[3].borrow_mut() = GridElement::USED;
    *line.elements[6].borrow_mut() = GridElement::USED;
    line._block_front_before_single_from_left(0);
    for _i in 0..3 {
        assert_eq!(true, GridElement::FREE == *line.elements[_i].borrow_mut());
    }
    line._block_front_before_single_from_left(0);
    for _i in 7..12 {
        assert_eq!(true, GridElement::BLOCKED == *line.elements[_i].borrow_mut());
    }
    line._block_front_before_single_from_right(0);
    assert_eq!(true, GridElement::BLOCKED == *line.elements[2].borrow_mut());

    for _i in 0..3 {
        assert_eq!(true, GridElement::BLOCKED == *line.elements[_i].borrow_mut());
    }
}