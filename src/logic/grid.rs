use std::fmt::{Display, Formatter};

use crate::logic::line::DELIMITER;
use crate::logic::line::Line;

pub struct Grid {
    size: usize,
    lines: Vec<Line>,
    has_changed : bool,
}

impl Display for Grid {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut out: String = String::new();
        // print rows with info
        for i in 0..self.size {
            out.push_str(&self.lines[i].to_string());
            out.push(' ');
            for n in self.lines[i].get_rectangle_info() {
                out.push_str(&n.to_string());
                out.push(DELIMITER);
            }
            out.push('\n');
        }
        // get maximum length for info for columns
        let mut max_vec_len = 0;
        for _line in self.size..2 * self.size {
            if self.lines[_line].get_rectangle_info().len() > max_vec_len {
                max_vec_len = self.lines[_line].get_rectangle_info().len();
            }
        }
        //out.push_str(&max_vec_len.to_string());

        // print info for rows
        for i in 0..max_vec_len {
            for _line in self.size..(2 * self.size) {
                if self.lines[_line].get_rectangle_info().len() > i {
                    out.push_str(&self.lines[_line].get_rectangle_info()[i].to_string())
                } else {
                    out.push(' ');
                }
                out.push(' ');
            }
            out.push('\n');
        }

        write!(f, "{}", out)
    }
}

impl Grid {
    pub fn new(size: usize, infos: Vec<Vec<usize>>) -> Self {
        let mut created_lines: Vec<Line> = Vec::new();
        for _line in 0..2 * size {
            let info = infos[_line].clone();
            created_lines.push(Line::new(size, _line, info, &created_lines))
        }
        Grid {
            size,
            lines: created_lines,
            has_changed : false,
        }
    }

    pub fn solve(&mut self) {
        println!("######################################################################################");
        println!("############# START ##################################################################");
        println!("######################################################################################");
        println!("{}", self.to_string());
        let mut round = 0;
        let mut is_change_in_round = true;
        while is_change_in_round  {
            // reset changes
            is_change_in_round = false;
            println!("######################################################################################");
            println!("############# Round: {} ", round);
            println!("######################################################################################");
            // strategy 1 rows
            self.has_changed = false;
            self.find_overlapping_in_row();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 1 columns
            self.has_changed = false;
            self.find_overlapping_in_col();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }

            // strategy 2 left
            self.has_changed = false;
            self.block_from_fixed_left();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 2 right
            self.has_changed = false;
            self.block_from_fixed_right();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 3
            self.has_changed = false;
            self.fix_max_rectangle();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 4 left
            self.has_changed = false;
            self.block_left_from_fixed();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 4 right
            self.has_changed = false;
            self.block_right_from_fixed();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 5 left
            self.has_changed = false;
            self.block_front_before_single_from_left();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            // strategy 5 right
            self.has_changed = false;
            self.block_front_before_single_from_right();
            if self.has_changed {
                is_change_in_round = true;
                println!("{}", self.to_string());
            }
            round += 1;
        }
        for line in self.lines.iter_mut() {
            line.reset_blocked_to_free();
        }
        println!("{}", self.to_string());
    }
}


// implementation of strategies
impl Grid {
    #[allow(dead_code)]
    pub fn find_overlapping(&mut self) {
        println!("processing rule: find overlapping");
        for line in self.lines.iter_mut() {
            line.find_overlapping();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn find_overlapping_in_row(&mut self) {
        println!("processing rule: find overlapping in rows");
        for _i in 0..self.size {
            self.lines[_i].find_overlapping();
            self.has_changed = self.has_changed || self.lines[_i].has_changed();
            self.lines[_i].set_has_changed(false);
        }
    }

    pub fn find_overlapping_in_col(&mut self) {
        println!("processing rule: find overlapping in columns");
        for _i in self.size..2 * self.size {
            self.lines[_i].find_overlapping();
            self.has_changed = self.has_changed || self.lines[_i].has_changed();
            self.lines[_i].set_has_changed(false);
        }
    }

    pub fn block_from_fixed_left(&mut self) {
        println!("processing rule: find fixable rectangle from: left to right | up to down");
        for line in self.lines.iter_mut() {
            line.find_fixable_rec_from_left_to_right();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn block_from_fixed_right(&mut self) {
        println!("processing rule: find fixable rectangle from: right to left | down to up");
        for line in self.lines.iter_mut() {
            line.find_fixable_rec_from_right_to_left();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn fix_max_rectangle(&mut self) {
        println!("processing rule: find max fixable rectangle in line");
        for line in self.lines.iter_mut() {
            line.find_max_fixable_rec();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn block_left_from_fixed(&mut self) {
        println!("processing rule: block all left | above from fixed rectangle");
        for line in self.lines.iter_mut() {
            line.block_left_from_rec();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn block_right_from_fixed(&mut self) {
        println!("processing rule: block all right | under from fixed rectangle");
        for line in self.lines.iter_mut() {
            line.block_right_from_rec();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn block_front_before_single_from_left(&mut self) {
        println!("processing rule: block in front | above USED/FIXED element");
        for line in self.lines.iter_mut() {
            line.block_front_before_single_from_left();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }

    pub fn block_front_before_single_from_right(&mut self) {
        println!("processing rule: block after | under USED/FIXED element");
        for line in self.lines.iter_mut() {
            line.block_front_before_single_from_right();
            self.has_changed = self.has_changed || line.has_changed();
            line.set_has_changed(false);
        }
    }
}