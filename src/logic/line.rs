use std::cell::RefCell;
use std::fmt::Display;
use std::rc::Rc;

mod tests;

pub const DELIMITER: char = ' ';
pub const SET_COLOR_BLACK: &str = "\x1b[0m";
//pub const SET_COLOR_RED: &str =  "\x1b[31m";
pub const SET_COLOR_GREEN: &str = "\x1b[32m";
pub const SET_COLOR_YELLOW: &str = "\x1b[33m";

/// GridElement:
/// Representation of one element in the grid
/// GridElementStatus:
/// represents all possible states of an element in the grid.
/// FREE < BLOCKED < USED < FIXES
#[derive(PartialOrd, Ord, PartialEq, Eq)]
enum GridElement {
    FREE,
    BLOCKED,
    USED,
    FIXED,
}

impl Display for GridElement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out: String = String::new();
        match self {
            GridElement::FREE => out.push('.'),
            GridElement::BLOCKED => out.push('x'),
            GridElement::USED => out.push('m'),
            GridElement::FIXED => out.push('M'),
        }
        write!(f, "{}", out)
    }
}

/// RecStatus:
/// represents all possible states about knowledge of a rectangle in one line
/// UNUSED < USED < FIXED
#[derive(PartialOrd, Ord, PartialEq, Eq)]
pub enum RecStatus {
    UNUSED,
    USED,
    FIXED,
}

/// RecInfo:
/// represents size and status of a rectangle in the grid
pub struct RecInfo {
    length: usize,
    pub status: RecStatus,
}

impl RecInfo {
    fn new(length: usize) -> Self {
        RecInfo {
            length,
            status: RecStatus::UNUSED,
        }
    }
}

impl Display for RecInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out = String::new();
        match self.status {
            RecStatus::UNUSED => out.push_str(SET_COLOR_BLACK),
            RecStatus::USED => out.push_str(SET_COLOR_YELLOW),
            RecStatus::FIXED => out.push_str(SET_COLOR_GREEN),
        }
        out.push_str(&self.length.to_string());
        out.push_str(SET_COLOR_BLACK);
        write!(f, "{}", out)
    }
}

/// Line
/// represent one line = row OR colum in the grid
/// example: line with line_length 8, and rec_info 1,1,2
///          and all GridElement as UNUSED
/// ........ 112
/// line_length: length of the line
/// has_changed: has line changed, with strategy applied to it
///  rec_info: list of lengths of all rectangles in line
///  elements: pointer to GridElements representing this line
pub struct Line {
    size: usize,
    has_changed: bool,
    rec_info: Vec<RecInfo>,
    elements: Vec<Rc<RefCell<GridElement>>>,
}

impl Line {
    pub fn new(size: usize, index: usize, rectangle_info: Vec<usize>, lines: &Vec<Line>) -> Self {
        let mut line: Vec<Rc<RefCell<GridElement>>> = Vec::new();
        if index < size {
            // create line with new Grid_Elements
            for _i in 0..size {
                line.push(Rc::new(RefCell::new(GridElement::FREE)))
            }
        } else {
            // create rows with references to existing lines
            let row_index = index - size;
            for _line_index in 0..size {
                line.push(lines[_line_index].elements[row_index].clone())
            }
        }
        let mut _info: Vec<RecInfo> = Vec::new();
        for info_element in rectangle_info.iter() {
            _info.push(RecInfo::new(*info_element));
        }
        Line {
            size,
            has_changed: false,
            rec_info: _info,
            elements: line,
        }
    }

    pub fn get_rectangle_info(&self) -> &Vec<RecInfo> {
        &self.rec_info
    }

    pub fn has_changed(&self) -> bool {
        self.has_changed
    }

    pub fn set_has_changed(&mut self, has_changed: bool) {
        self.has_changed = has_changed;
    }

    pub fn reset_blocked_to_free(&mut self) {
        for elem in self.elements
            .iter()
            .filter(|e| *e.borrow() == GridElement::BLOCKED) {
            //println!("{}",elem.borrow().status.to_string());
            *elem.borrow_mut() = GridElement::FREE;
        }
    }
}

impl Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out: String = String::new();
        for i in 0..self.size {
            out.push_str(&self.elements[i].borrow().to_string().clone());
            out.push(DELIMITER);
        }
        write!(f, "{}", out)
    }
}

// implementation of logic
impl Line {
    #[allow(dead_code)]
    fn get_minimal_marketplace_size(&self) -> usize {
        // all empty spaces between elements
        let mut min_size = self.rec_info.len() - 1;
        // add all elements
        min_size += self.rec_info.iter().map(|x| x.length).sum::<usize>();
        min_size
    }

    fn single_not_fixed(&self, i: usize) -> bool {
        self.rec_info.iter().filter(|r| r.status < RecStatus::FIXED ).count() == 1
            && self.rec_info[i].status < RecStatus::FIXED
    }

    /// Recursive search for most left starting point for rectangle(i)
    #[allow(dead_code)]
    fn rec_most_left_start(&self, i: usize) -> usize {
        // start most left
        let mut possible_start = 0;
        if i > 0 {
            // get most left for rec's before
            possible_start = self.rec_most_left_start(i - 1) + self.rec_info[i - 1].length + 1;
        }
        // check, if BLOCKED moves rec to right
        let mut increasing_rec_len = 0;
        while increasing_rec_len < self.rec_info[i].length {
            // BLOCKED in rectangle => move after BLOCKED
            if *self.elements[possible_start + increasing_rec_len].borrow() == GridElement::BLOCKED {
                // increasing_rec_len + 1 has to be in line, because search from left!
                possible_start += increasing_rec_len + 1;
                increasing_rec_len = 0;
            } else {
                increasing_rec_len += 1;
            }
            // USED or FIXED after rectangle => move rectangle +1 to right to reach USED or FIXED
            // increasing_rec_len reaches after rectangle
            if increasing_rec_len == self.rec_info[i].length &&
                possible_start + increasing_rec_len < self.size &&
                *self.elements[possible_start + increasing_rec_len].borrow() >= GridElement::USED {
                possible_start += 1;
                increasing_rec_len = 0;
            }
        }
        // search left from start for possible_start for start, if FIXED
        if self.rec_info[i].status == RecStatus::FIXED {
            let mut cursor = possible_start;
            while *self.elements[cursor].borrow() != GridElement::FIXED {
                cursor += 1;
            }
            possible_start = cursor;
        }
        possible_start
    }

    /// Recursive search for most right starting point for rectangle(i)
    fn rec_most_right_start(&self, i: usize) -> usize {
        // start most right
        let mut possible_start = self.size - 1;
        if i < self.rec_info.len() - 1 {
            possible_start = self.rec_most_right_start(i + 1) - self.rec_info[i + 1].length - 1;
        }
        let mut increasing_rec_len = 0;
        while increasing_rec_len < self.rec_info[i].length {
            if *self.elements[possible_start - increasing_rec_len].borrow() == GridElement::BLOCKED {
                possible_start = possible_start - increasing_rec_len - 1;
                increasing_rec_len = 0;
            } else {
                increasing_rec_len += 1;
            }
            // USED or FIXED before rectangle => move rectangle 1 to left to reach USED or FIXED
            // increasing_rec_len looks after rectangle
            if increasing_rec_len == self.rec_info[i].length &&
                possible_start >= increasing_rec_len &&
                *self.elements[possible_start - increasing_rec_len].borrow() >= GridElement::USED {
                possible_start -= 1;
                increasing_rec_len = 0;
            }
        }
        // search left from start for possible_start for start, if FIXED
        if self.rec_info[i].status == RecStatus::FIXED {
            let mut cursor = possible_start;
            while *self.elements[cursor].borrow() != GridElement::FIXED {
                // println!("{} {}", i, cursor);
                cursor -= 1;
            }
            possible_start = cursor;
        }
        possible_start
    }

    /// Maximal space for rectangle(i) -> (start, length)
    /// was useful for testing
    #[allow(dead_code)]
    fn max_space_for_rec(&self, i: usize) -> (usize, usize) {
        // self.get_most_right
        // t_start_for_rectangle(i) - self.get_most_left_start_for_rectangle(i)
        // calculates steps from left to right
        // + 1 to add start position maximal space
        // 0123456789
        // .....x....
        // 12345 but 4 - 0 = 4
        let possible_left = self.rec_most_left_start(i);
        let possible_right = self.rec_most_right_start(i);
        (possible_left, possible_right - possible_left + 1)
    }

    /// Start and length of overlapping rectangle(i) -> Some(start, length)
    fn overlapping_for_rec(&self, i: usize) -> Option<(usize, usize)> {
        /*
            0123456789
            xxx......x maximal_space_for_rec = 6
            xxxmmmm..x overlapping = 2 * 4 - 6 = 2
            xxx..mmmmx
            xxxffooffx left_free 6 - 2 = 4 for left and right => 4/2 = for left
        */
        /*
            old version with self.max_space_for_rec(i)
            if self.max_space_for_rec(i).1 >= 2 * self.rec_info[i].length {
                return None;
            }
            let overlapping = 2 * self.rec_info[i].length - self.max_space_for_rec(i).1;
            let left_free = (self.max_space_for_rec(i).1 - overlapping) / 2;
            Some((self.max_space_for_rec(i).0 + left_free, overlapping))
        */
        let max_space_for_rec = self.rec_most_right_start(i) - self.rec_most_left_start(i) + 1;
        if max_space_for_rec >= 2 * self.rec_info[i].length {
            return None;
        }
        let overlapping = 2 * self.rec_info[i].length - max_space_for_rec;
        let left_free = (max_space_for_rec - overlapping) / 2;
        Some((self.rec_most_left_start(i) + left_free, overlapping))
    }

    /// Public Strategy:
    /// find overlapping
    pub fn find_overlapping(&mut self) {
        for i in 0..self.rec_info.len() {
            if self.rec_info[i].status == RecStatus::FIXED {
                continue;
            }
            if let Some((start, overlapping)) = self.overlapping_for_rec(i) {
                if self.rec_info[i].status == RecStatus::UNUSED {
                    self.rec_info[i].status = RecStatus::USED;
                }
                for _i in start..start + overlapping {
                    if *self.elements[_i].borrow() == GridElement::FREE {
                        *self.elements[_i].borrow_mut() = GridElement::USED;
                        self.has_changed = true;
                    }
                }
            }
        }
    }

    /// Find element from left to right, which must be assumed as fix
    /// we try to get first none FIXED rec from left to right
    /// if there is a USED/FIXED on rec_most_left_start(i), we return start position
    /// if we find a section with 'rec.size' and USED/FIXED in it, we return start position
    /// e.g.
    /// x x . m x . . . . . . .  2 3 => 2 can be fixed
    fn find_fixable_rec_from_left(&self, i: usize) -> Option<usize> {
        if self.rec_info[i].status == RecStatus::FIXED {
            return None;
        }
        // to FIX i, all rectangles before must be fixed
        for before in 0..i {
            if self.rec_info[before].status != RecStatus::FIXED {
                return None;
            }
        }
        if *self.elements[self.rec_most_left_start(i)].borrow() >= GridElement::USED {
            Some(self.rec_most_left_start(i))
        } else {
        let right = self.rec_most_left_start(i) + self.rec_info[i].length;
            if  right == self.size || *self.elements[right].borrow() == GridElement::BLOCKED {
                let mut found = false;
                for cursor in self.rec_most_left_start(i)..right {
                    found = found || *self.elements[cursor].borrow() >= GridElement::USED;
                }
                if found {
                    return Some(self.rec_most_left_start(i))
                }
            }
            None
        }
    }

    /// Find element from right to left, which must be assumed as fix
    /// same as: find_fixable_rec_from_left
    fn find_fixable_rec_from_right(&self, i: usize) -> Option<usize> {
        if self.rec_info[i].status == RecStatus::FIXED {
            return None;
        }
        for after in ((i + 1)..self.rec_info.len()).rev() {
            if self.rec_info[after].status != RecStatus::FIXED {
                return None;
            }
        }
        if *self.elements[self.rec_most_right_start(i)].borrow() >= GridElement::USED {
            Some(self.rec_most_right_start(i))
        } else {
            let left = self.rec_most_right_start(i) - self.rec_info[i].length;
            //if  self.rec_most_right_start(i) - self.rec_info[i].length < 0 || self.elements[left].borrow().status == GridElementStatus::BLOCKED {
            if  *self.elements[left].borrow() == GridElement::BLOCKED {
                let mut found = false;
                for cursor in left + 1..self.rec_most_right_start(i) {
                    found = found || *self.elements[cursor].borrow() >= GridElement::USED;
                }
                if found {
                    return Some(self.rec_most_right_start(i))
                }
            }
            None
        }
    }


    /// Public Strategy:
    /// find fixable rectangles from left to right
    pub fn find_fixable_rec_from_left_to_right(&mut self) {
        for i in 0..self.rec_info.len() {
            if let Some(start) = self.find_fixable_rec_from_left(i) {
                // mark RectangleStatus as FIXED
                self.rec_info[i].status = RecStatus::FIXED;
                // block before rec
                if start > 0 {
                    *self.elements[start - 1].borrow_mut() = GridElement::BLOCKED;
                }
                // fix rec
                for _i in start..start + self.rec_info[i].length {
                    *self.elements[_i].borrow_mut() = GridElement::FIXED;
                }
                // block after rec
                if start + self.rec_info[i].length < self.size {
                    *self.elements[start + self.rec_info[i].length].borrow_mut() = GridElement::BLOCKED;
                }
                self.has_changed = true;
            }
        }
    }


    /// Public Strategy:
    /// find fixable rectangles from right to left
    pub fn find_fixable_rec_from_right_to_left(&mut self) {
        for i in (0..self.rec_info.len()).rev() {
            if let Some(start) = self.find_fixable_rec_from_right(i) {
                // mark RectangleStatus as FIXED
                self.rec_info[i].status = RecStatus::FIXED;
                // block right from rec
                if start + 1 < self.size {
                    *self.elements[start + 1].borrow_mut() = GridElement::BLOCKED;
                }
                // set rec to FIXED
                for _i in (start + 1 - self.rec_info[i].length)..start + 1 {
                    *self.elements[_i].borrow_mut() = GridElement::FIXED;
                }
                // block left from rec
                if start > self.rec_info[i].length {
                    *self.elements[start - self.rec_info[i].length].borrow_mut() = GridElement::BLOCKED;
                }
                self.has_changed = true;
            }
        }
    }

    //  broken version because
    //  mutable borrow in: self.rectangle_info.iter_mut().enumerate().rev()
    //  and immutable borrow in self.find_fixed_from_right(i)
    //  pub fn block_from_fixed_right2(&mut self) {
    //        for (i, rectangle) in self.rectangle_info.iter_mut().enumerate().rev() {
    //            if let Some(start) = self.find_fixed_from_right(i) {
    //                // mark RectangleStatus as FIXED
    //                //*rectangle.status.borrow_mut() = RectangleStatus::FIXED;
    //                rectangle.status = RectangleStatus::FIXED;
    //                self.has_changed = true;
    //                for _i in (start + 1 - rectangle.length)..start + 1 {
    //                    self.elements[_i].borrow_mut().status = GridElementStatus::FIXED;
    //                }
    //                self.elements[start - rectangle.length].borrow_mut().status = GridElementStatus::BLOCKED;
    //            }
    //        }
    //    }


    fn max_not_fixed_rec_length(&self) -> Option<(usize, usize)> {
        let mut max: Option<(usize, usize)> = None;
        for r_info in self.rec_info.iter().filter(|r| r.status != RecStatus::FIXED) {
            match max {
                None => max = Some((r_info.length, 1)),
                Some((_, _)) => if r_info.length > max.unwrap().0 {
                    max = Some((r_info.length, 1));
                } else if r_info.length == max.unwrap().0 {
                    max = Some((max.unwrap().0, max.unwrap().1 + 1));
                }
            }
        }
        max
    }

    /// Public Strategy:
    /// check if longest rectangle in line can be set to FIX
    pub fn find_max_fixable_rec(&mut self) {
        if let Some((len, _count)) = self.max_not_fixed_rec_length() {
            // look for rectangles in elements
            for (i, elem) in self.elements.iter().enumerate() {
                if *elem.borrow() == GridElement::FREE ||
                    *elem.borrow() == GridElement::BLOCKED {
                    continue;
                }
                // we did not start with space and USED or FIXED
                if i > 0 && (*self.elements[i - 1].borrow() == GridElement::USED ||
                    *self.elements[i - 1].borrow() == GridElement::FIXED) {
                    continue;
                }
                // rectangle will not fit in line anymore
                if i + len >= self.size {
                    break;
                }
                let mut found = true;
                // check for FREE or BLOCKED in rectangle: i, i+1, ... i + len - 1
                for following in i..i + len {
                    if *self.elements[following].borrow() == GridElement::FREE ||
                        *self.elements[following].borrow() == GridElement::BLOCKED {
                        found = false;
                    }
                }
                // check for USED or FIXED after rectangle i + len
                if found && i + len < self.size &&
                    (*self.elements[i + len].borrow() == GridElement::USED ||
                        *self.elements[i + len].borrow() == GridElement::FIXED) {
                    found = false;
                }
                if found {
                    // block before
                    if i > 0
                        && *self.elements[i - 1].borrow_mut() != GridElement::BLOCKED {
                        self.has_changed = true;
                        *self.elements[i - 1].borrow_mut() = GridElement::BLOCKED
                    }
                    // block after
                    if i + len < self.size
                        && *self.elements[i + len].borrow_mut() != GridElement::BLOCKED {
                        self.has_changed = true;
                        *self.elements[i + len].borrow_mut() = GridElement::BLOCKED
                    }
                }
            }
        }
    }

    fn block_before_rec(&mut self, i: usize) {
        let mut from_left = 0;
        if i > 0 {
            from_left = self.rec_most_right_start(i - 1) + 1;
        }
        for _i in from_left..self.rec_most_left_start(i) {
            if *self.elements[_i].borrow() == GridElement::FREE {
                self.has_changed = true;
                *self.elements[_i].borrow_mut() = GridElement::BLOCKED;
            }
        }
    }

    /// Public Strategy:
    /// block left from fixed
    pub fn block_left_from_rec(&mut self) {
        // try blocking from left
        for i in 0..self.rec_info.len() {
            self.block_before_rec(i);
        }
    }

    fn block_after_rec(&mut self, i: usize) {
        let mut from_right = self.size;
        if i < self.rec_info.len() - 1 {
            from_right = self.rec_most_left_start(i + 1);
        }
        for _i in self.rec_most_right_start(i) + 1..from_right {
            if *self.elements[_i].borrow() == GridElement::FREE {
                self.has_changed = true;
                *self.elements[_i].borrow_mut() = GridElement::BLOCKED;
            }
        }
    }

    /// Public Strategy:
    /// block right from fixed
    pub fn block_right_from_rec(&mut self) {
        // try blocking from left
        for i in (0..self.rec_info.len()).rev() {
            self.block_after_rec(i);
        }
    }

    fn _block_front_before_single_from_left(&mut self, i: usize) {
        if self.single_not_fixed(i) {
            let left = self.rec_most_left_start(i);
            let right = self.rec_most_right_start(i);
            let mut cursor = left;
            // lok for USED/FIXED in range
            while cursor <= right && *self.elements[cursor].borrow() < GridElement::USED {
                cursor += 1;
            }
            if cursor < self.size && *self.elements[cursor].borrow() >= GridElement::USED {
                // block from found USED/FIXED with radius length
                cursor += self.rec_info[i].length;
                // length = 3
                // . . . .  . .v <- cursor
                // . . . M 1 2 3
                // . . . M M M x <- block from here
                while cursor < self.size {
                    if *self.elements[cursor].borrow() == GridElement::FREE {
                        *self.elements[cursor].borrow_mut() = GridElement::BLOCKED;
                        self.has_changed = true;
                    }
                    cursor += 1;
                }
            }
        }
    }

    /// Public Strategy:
    /// block right from fixed
    pub fn block_front_before_single_from_left(&mut self) {
        for i in 0..self.rec_info.len() {
            self._block_front_before_single_from_left(i);
        }
    }

    fn _block_front_before_single_from_right(&mut self, i: usize) {
        if self.single_not_fixed(i) {
            let left = self.rec_most_left_start(i);
            let right = self.rec_most_right_start(i);
            let mut cursor = right;
            // look for USED/FIXED in range
            while cursor > left && *self.elements[cursor].borrow() < GridElement::USED {
                cursor -= 1;
            }
            if cursor >= self.rec_info[i].length && *self.elements[cursor].borrow() >= GridElement::USED {
                // block from found USED/FIXED with radius length
                cursor -= self.rec_info[i].length - 1;
                // length = 3
                // . . . . v . . <- cursor
                // . . . 3 2 1 M
                // . . . x M M M . <- block from here
                while cursor > 0 {
                    cursor -= 1;
                    if *self.elements[cursor].borrow() == GridElement::FREE {
                        *self.elements[cursor].borrow_mut() = GridElement::BLOCKED;
                        self.has_changed = true;
                    }
                }
            }
        }
    }

    /// Public Strategy:
    /// block right from fixed
    pub fn block_front_before_single_from_right(&mut self) {
        for i in 0..self.rec_info.len() {
            self._block_front_before_single_from_right(i);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

