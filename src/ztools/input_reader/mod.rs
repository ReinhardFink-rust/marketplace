use std::fs;

pub fn to_usize_vec(string : &String) -> Vec<usize> {
    let mut usize_vec: Vec<usize> = Vec::new();
    let mut digits = String::new();
    for c in string.chars() {
        if c.eq(&' ') {
            usize_vec.push(digits.parse().unwrap());
            digits.clear();
        }
        else {
            digits.push(c);
        }
    }
    usize_vec.push(digits.parse().unwrap());
    usize_vec
}

pub struct InputString {
    input_string : String,
    cursor : usize,
}

impl InputString {
    pub fn new(_file_name : &str) -> Self {
        let string = fs::read_to_string(_file_name).unwrap_or("Could not transfer file to String".to_string());
        InputString {
            input_string : string,
            cursor : 0,
        }
    }

    pub fn next_line(&mut self) -> Option<String> {
        let mut line = String::new();
        // check Some() because we get None at unexpected end of file
        while let Some(tmp_str) = self.input_string.get(self.cursor..self.cursor + 1) {
            //println!("{}",tmp_str);
            if tmp_str.eq("\n") {
                // jump over /n
                self.cursor += 1;
                break;
            }
            line.push_str(tmp_str);
            self.cursor += 1;
        }
        if line.is_empty() {
            Option::None
        }
        else {
            Option::Some(line)
        }
    }
}
