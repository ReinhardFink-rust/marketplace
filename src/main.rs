use crate::logic::grid::Grid;
use crate::ztools::input_reader::InputString;
use crate::ztools::input_reader::to_usize_vec;

mod ztools;
mod logic;

fn main() {
    //solve_file("marketplace001.txt");
    solve_file("marketplace051.txt");
    //solve_file("marketplace051.txt");
}

fn solve_file(filename: &str) {
    println!("Marketplace {}", filename);
    let mut file_path : String = String::from("src/data/");
    file_path.push_str(filename);
    let mut input = InputString::new(&file_path);
    let mut infos : Vec<Vec<usize>> = Vec::new();
    while let Some(line) = input.next_line() {
        infos.push(to_usize_vec(&line))
    }
    let mut mp = Grid::new(infos.len()/2, infos);
    mp.solve();
}
